#ifndef STREAM9_PARTIAL_ERROR_TEST_SRC_ERROR_HPP
#define STREAM9_PARTIAL_ERROR_TEST_SRC_ERROR_HPP

#include <string>
#include <system_error>

namespace testing {

enum class errc {
    error1,
    error2,
};

std::error_category const&
error_category()
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept
        {
            return "category_name";
        }

        std::string message(int const e) const
        {
            switch (static_cast<errc>(e)) {
                case errc::error1:
                    return "error1";
                case errc::error2:
                    return "error2";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

std::error_code
make_error_code(errc const e)
{
    return { static_cast<int>(e), error_category() };
}

} // namespace testing

namespace std {

template<>
struct is_error_code_enum<testing::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_PARTIAL_ERROR_TEST_SRC_ERROR_HPP
